from app.gm import GameManager
import sys

if __name__ == '__main__':

    gm = GameManager()
    gm.init()
    gm.run()

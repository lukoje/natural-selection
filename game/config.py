
class Screen:
    WIDTH = 1280
    HEIGHT = 720
    RESOLUTION = (WIDTH, HEIGHT)
    FULLSCREEN = False
    FPS = 60
    BOUNDARY_SIZE = 10

class Colors:
    BLACK = (0,0,0)
    WHITE = (255,255,255)

    RED = (255, 0, 0)
    GREEN = (0, 255, 0)
    BLUE = (0, 0, 255)

    GREY = (100, 100, 100)
    LIGHT_GREY = (240, 240, 240)
    DARK_GREY = (30, 30, 50)

    BGCOLOR = BLACK

class FontDefaults:
    FONT_FAMILY = 'Consolas'
    FONT_SIZE = 16


class HostDefaults:
    SIZE = 5
    MAX_VEL = 3
    COLOR = Colors.GREEN
    ENERGY = 100
    

class FoodDefaults:
    SIZE = 2
    COLOR = Colors.LIGHT_GREY




